"""
task_pub_table_order URL Configuration
"""

from django.contrib import admin
from django.urls import path, include

from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="Table Order API",
      default_version='v1',
   ),
   public=True,
)

api_urlpatterns = [
    path('orders/', include('apps.order.urls', namespace='order')),
    path('infrastructure/', include('apps.infrastructure.urls', namespace='infrastructure'))
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_urlpatterns)),
]

urlpatterns += [
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
