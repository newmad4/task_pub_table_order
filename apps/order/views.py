from rest_framework.generics import CreateAPIView

from order.models import Order
from order.serializers import OrderCreateSerializer


__all__ = [
    'OrderCreateAPIView'
]


class OrderCreateAPIView(CreateAPIView):
    """
    Allow create order for user
    """
    queryset = Order.objects.all()
    serializer_class = OrderCreateSerializer
