from django.conf import settings
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver

from order.models import Order


@receiver(post_save, sender=Order)
def send_mail_order_confirmation(sender, instance, created, **kwargs):
    # TODO: this logic must be in celery task, for example
    if created:
        subject = 'You create order in our place'
        message = 'Congratulations - you make successfully order for table in our place'
        status = send_mail(subject, message, settings.EMAIL_HOST_USER, [instance.email], fail_silently=False)
        instance.is_email_sent = status
        instance.save()
