from django.utils import timezone

from django.core.validators import MinValueValidator
from django.db import models

from infrastructure.models import Table


class Order(models.Model):
    """
    Model for order
    """
    dt_created = models.DateTimeField(auto_now_add=True)
    dt_updated = models.DateTimeField(auto_now=True)

    booked_from = models.DateTimeField(
        help_text="Date and time when client order begin",
        validators=[MinValueValidator(timezone.now())]  # This order must be in future - not in past
    )
    booked_to = models.DateTimeField(
        help_text="Date and time when client order end",
        validators=[MinValueValidator(timezone.now())]  # This order must be in future - not in past
    )

    table = models.ForeignKey(
        Table, related_name="orders", on_delete=models.CASCADE, help_text="What table client booked"
    )
    KINDS = (
        ('IN_PROGRESS', 'In progress'),  # for order which must be happened or used in current moment
        ('DONE', 'Done'),  # for successfully done order
        ('ABORTED', 'Aborted'),  # aborted by client
        ('DECLINED', 'Declined'),  # declined by company manager in some technical reason reason
    )
    status = models.CharField(
        verbose_name='status', choices=KINDS, max_length=50, help_text="Status of order", default='IN_PROGRESS'
    )

    email = models.EmailField(help_text="Client email")
    name = models.CharField(help_text="Client name", max_length=50)
    is_email_sent = models.BooleanField(
        help_text="Indicates that email with order information was sent to client", null=True, blank=True
    )
