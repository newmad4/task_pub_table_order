
from django.core.exceptions import ValidationError
from rest_framework import serializers

from order.models import Order
from order.utils import could_not_booking

__all__ = [
    'OrderCreateSerializer'
]


class OrderCreateSerializer(serializers.ModelSerializer):
    """
    Order model serializer
    """

    class Meta:
        model = Order
        fields = ('id', 'booked_from', 'booked_to', 'table', 'email', 'name')

    def validate(self, data):
        """
        Make datetime validation and check booking possibility
        """
        start_date_time = data['booked_from']
        end_date_time = data['booked_to']

        if start_date_time >= end_date_time:
            raise ValidationError({'booking datetime range': "Booking end datetime must occur after start datetime"})

        if could_not_booking(data['table'], start_date_time, end_date_time):
            raise ValidationError({'booking datetime range': "This table is busy in chosen time range"})
        return data
