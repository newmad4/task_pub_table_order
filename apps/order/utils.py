from datetime import datetime

from django.db.models import Q

from order.models import Order


def could_not_booking(table_id: int, start_date_time: datetime, end_date_time: datetime) -> bool:
    """
    Check if table is busy in time range
    """
    return Order.objects.filter(status='IN_PROGRESS', table_id=table_id).filter(
        (Q(booked_from__range=[start_date_time, end_date_time]) | Q(booked_to__range=[start_date_time, end_date_time]))
    ).exists()
