from rest_framework.generics import ListAPIView

from infrastructure.models import Hall, Table
from infrastructure.serializers import HallSerializer, TableSerializer


__all__ = [
    'HallListAPIView',
    'TablesListAPIView'
]


class HallListAPIView(ListAPIView):
    """
    Return all halls in institution
    """
    serializer_class = HallSerializer
    queryset = Hall.objects.all()


class TablesListAPIView(ListAPIView):
    """
    Return all tables from chosen hall
    """
    serializer_class = TableSerializer

    def get_queryset(self):
        return Table.objects.filter(hall_id=self.kwargs['hall_pk'])
