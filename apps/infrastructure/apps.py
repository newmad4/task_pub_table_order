from django.apps import AppConfig


class TableOrderConfig(AppConfig):
    name = 'infrastructure'
