from django.db import models


__all__ = [
    'Hall',
    'Table',
]


class Hall(models.Model):
    """
    Model of hall
    """
    description = models.TextField(null=True, blank=True, help_text="Some hall description")
    length = models.FloatField(help_text="Length in meters of hall")
    width = models.FloatField(help_text="Width in meters of hall")


class Table(models.Model):
    """
    Model for each table in any halls
    """
    hall = models.ForeignKey(
        Hall, related_name='tables', on_delete=models.CASCADE, help_text="Link to hall model from where this table"
    )
    number = models.IntegerField(help_text="number of current table")
    number_of_places = models.IntegerField(help_text="How many persons can seat around this table")

    KINDS = (
        ('RECTANGLE', 'Rectangle'),
        ('ELLIPSE', 'Ellipse')
    )
    shape = models.CharField(verbose_name='shape', max_length=50, choices=KINDS, help_text="Table shape")

    center_coordinate_x = models.FloatField(help_text="X Coordinate of table center")
    center_coordinate_y = models.FloatField(help_text="Y Coordinate of table center")

    length = models.FloatField(help_text="Length of table")
    width = models.FloatField(help_text="Width of table")
