import datetime

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from infrastructure.models import Hall, Table
from order.utils import could_not_booking

__all__ = [
    'HallSerializer',
    'TableSerializer'
]


class HallSerializer(serializers.ModelSerializer):
    """
    Hall model serializer
    """

    class Meta:
        model = Hall
        fields = ('id',)


class TableSerializer(serializers.ModelSerializer):
    """
    Table model serializer
    """
    status = serializers.SerializerMethodField()

    class Meta:
        model = Table
        fields = ('id', 'number', 'status')

    def get_status(self, obj):
        """
        Return dynamically calculated status depended of datetime booking range and history orders of this table
        """
        # get query params
        start_date_time = self.context['request'].GET.get('start_date_time')
        end_date_time = self.context['request'].GET.get('end_date_time')

        if not (start_date_time and end_date_time):
            raise ValidationError("You must provide start_date_time and end_date_time in query parameters")

        try:
            start_date_time = datetime.datetime.fromisoformat(start_date_time)
            end_date_time = datetime.datetime.fromisoformat(end_date_time)
        except (ValueError, TypeError):
            raise ValidationError("Provided data is not in ISO datetime format")
        if start_date_time >= end_date_time:
            raise ValidationError("Booking end datetime must occur after start datetime")

        # Check if there are no opening orders to current table in such datetime range
        if not could_not_booking(obj.pk, start_date_time, end_date_time):
            return 'free'

        # table would be closed if order exists in this time range
        return 'closed'
