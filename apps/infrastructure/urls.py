from django.urls import path

from infrastructure.views import HallListAPIView, TablesListAPIView

app_name = 'infrastructure'

urlpatterns = [
    path('halls/', HallListAPIView.as_view(), name='hall-list'),
    path('tables/<int:hall_pk>/', TablesListAPIView.as_view(), name='tables-list'),
]
