# Home task: Application for booking table in some institution

Technical tasks: 
1) Django/Django REST Framework
2) Only REST API, without frontend
3) Without authentication and authorization
4) API for create order by user
5) Models: Hall, Table, Order (with some attributes from SRS)
6) Send email notification to user when order was successfully created into console
7) Without admin panel models registration
8) For first time without Celery

Was used Python 3.8
